#include "userwindow.h"
#include "ui_userwindow.h"
#include "payment.h"
#include "database.h"
#include <QLabel>
#include <QLayoutItem>
#include <QButtonGroup>

UserWindow::UserWindow(Database *mysql) :
    QMainWindow(nullptr),
    ui(new Ui::UserWindow)
{
    ui->setupUi(this);
    ui->listWidget->addItem("Gatunki:");
    for (QString kategoria:this->gatunek)
    {
        ui->listWidget->addItem(kategoria);

    }
    wczytaneFilmy = mysql->wczytajFilmy();
    QButtonGroup *buttns =  new QButtonGroup();
    for(auto film : wczytaneFilmy)
    {
        //ui->rok->setText("Rok Produkcji: " + film.rok);
        QLabel *tep =  new QLabel(); //rem to delete pointers
        QImage pix (film.img);
        tep->setPixmap(QPixmap::fromImage(pix.scaled(250, 350), Qt::AutoColor));
        ui->gridLayout->addWidget(tep);
         //rem to delete pointers
//        QAbstractButton *temp2 = new QAbstractButton();
//        buttns->addButton()

    }



//    QImage pix ("/home/krzysiek/QtProjekt/zdjecia filmow/incepcja.png");
//    QImage pix2 ("/home/krzysiek/QtProjekt/zdjecia filmow/nietykalni.png");
//    QImage pix3 ("/home/krzysiek/QtProjekt/zdjecia filmow/obcy8.png");
//    QImage pix4 ("/home/krzysiek/QtProjekt/zdjecia filmow/podziemnykrag.png");
//    QImage pix5 ("/home/krzysiek/QtProjekt/zdjecia filmow/skazaninashawhank.png");
//    ui->film1->setPixmap(QPixmap::fromImage(pix.scaled(250, 350), Qt::AutoColor));
//    ui->film2->setPixmap(QPixmap::fromImage(pix2.scaled(250, 350), Qt::AutoColor));
//    ui->film3->setPixmap(QPixmap::fromImage(pix3.scaled(250, 350), Qt::AutoColor));
//    ui->film4->setPixmap(QPixmap::fromImage(pix4.scaled(250, 350), Qt::AutoColor));
//    ui->film5->setPixmap(QPixmap::fromImage(pix5.scaled(250, 350), Qt::AutoColor));
}

UserWindow::~UserWindow()
{
    delete ui;
}

void UserWindow::on_info5_clicked()
{
    ui->rok->setText("Rok produkcji: 1999");
    ui->rezyser->setText("Reżyser: Frank Darabont");
    ui->gatunek->setText("Gatunek: Dramat");
    ui->ocena->setText("Ocena widzów: 8.8/10");
    ui->cena->setText("Cena: 25zl");
    ui->tekst->setText("Adaptacja opowiadania Stephena Kinga. Niesłusznie skazany na dożywocie bankier, stara się przetrwać w brutalnym, więziennym świecie.");
    wybrany_film = "Skazani na shawshank";
    cena = 25;
}


void UserWindow::on_info4_clicked()
{
    ui->rok->setText("Rok produkcji: 1999");
    ui->rezyser->setText("Reżyser: David Fincher");
    ui->gatunek->setText("Gatunek: Thriller");
    ui->ocena->setText("Ocena widzów: 8.3/10");
    ui->cena->setText("Cena: 20zl");
    ui->tekst->setText("Dwóch mężczyzn znudzonych rutyną zakłada klub, w którym co tydzień odbywają się walki na gołe pięści.");
    wybrany_film = "Podziemny krąg";
    cena = 20;

}


void UserWindow::on_info3_clicked()
{
    ui->rok->setText("Rok produkcji: 1997");
    ui->rezyser->setText("Reżyser: Ridley Scott");
    ui->gatunek->setText("Gatunek: Horror");
    ui->ocena->setText("Ocena widzów: 7.9/10");
    ui->cena->setText("Cena: 15zl");
    ui->tekst->setText("Załoga statku kosmicznego Nostromo odbiera tajemniczy sygnał i ląduje na niewielkiej planetoidzie, gdzie jeden z jej członków zostaje zaatakowany przez obcą formę życia.");
    wybrany_film = "Obcy- 8";
    cena = 15;
}


void UserWindow::on_info2_clicked()
{
    ui->rok->setText("Rok produkcji: 2011");
    ui->rezyser->setText("Reżyser: Olivier Nakache");
    ui->gatunek->setText("Gatunek: Komedia");
    ui->ocena->setText("Ocena widzów: 8.7/10");
    ui->cena->setText("Cena: 25zl");
    ui->tekst->setText("Sparaliżowany milioner zatrudnia do opieki młodego chłopaka z przedmieścia, który właśnie wyszedł z więzienia.");
    wybrany_film = "Nietykalni";
    cena = 25;

}


void UserWindow::on_info1_clicked()
{
    ui->rok->setText("Rok produkcji: 2010");
    ui->rezyser->setText("Reżyser: Christopher Nolan");
    ui->gatunek->setText("Gatunek: Sci-Fi");
    ui->ocena->setText("Ocena widzów: 8.3/10");
    ui->cena->setText("Cena: 25zl");
    ui->tekst->setText("Czasy, gdy technologia pozwala na wchodzenie w świat snów. Złodziej Cobb ma za zadanie wszczepić myśl do śpiącego umysłu.");
    wybrany_film = "Incepcja";
    cena = 25;

}


void UserWindow::on_pushButton_2_clicked()
{
    paymentWindow = new Payment(this, wybrany_film, cena);
    paymentWindow->show();
}


