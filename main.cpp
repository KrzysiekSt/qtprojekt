
#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QDebug>

#include "appmanager.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;

    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "QtProjekt_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }

    AppManager instance;

    return a.exec();
}
