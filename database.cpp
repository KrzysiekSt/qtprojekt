#include "database.h"

Database::Database()
{
    isAdmin = false;
    database = QSqlDatabase::addDatabase("QMYSQL");

    database.setHostName("sql11.freesqldatabase.com");
    database.setUserName("sql11470013");
    database.setDatabaseName("sql11470013");
    database.setPassword("nC9juZkHLJ");

    if(!database.open())
    {
        qDebug() << database.lastError().text();
    }
    else
    {
        qDebug() << "Database opened!";
    }

}

bool Database::haveAdmin()
{
    QSqlQuery haveAdmin("SELECT `isAdmin` FROM QAWSED WHERE `isAdmin`=1;", database);

    if(haveAdmin.exec())
    {
        if(haveAdmin.size() > 0)
        {
            return true;
        }
    }
    else
    {
        qDebug() << haveAdmin.lastError().text();
    }
    return false;
}

bool Database::createAdmin()
{
    qDebug() << "creating Admin!!!!!!";
    QSqlQuery createAdmin("INSERT INTO `QAWSED` (`id`, `email`, `password`, `isAdmin`) VALUES ('1', 'admin', 'admin', '1');", database);

    if(createAdmin.exec())
    {
        return true;
    }
    else
    {
        qDebug() << createAdmin.lastError().text();
    }
    return false;
}

bool Database::handleAdminAccount()
{
    if(!haveAdmin())
    {
        if(!createAdmin())
        {
            return false;
        }
    }
    return true;
}


bool Database::tryLogin(Ui::Credentials &login)
{
    QSqlQuery tryLogin("SELECT `email`, `password`, `isAdmin` FROM `QAWSED` WHERE `email`='" +
                       login.email + "' AND `password`='" + login.password + "'", database);

    if(tryLogin.exec())
    {
        if(tryLogin.size() > 0)
        {
            qDebug() << tryLogin.size();
            tryLogin.first();
            isAdmin = (tryLogin.value("isAdmin").toString() == "1") ? true : false;
            qDebug() << tryLogin.value("isAdmin").toString();
            return true;
        }
    }
    else
    {
        qDebug() << tryLogin.lastError().text();
    }
    return false;

}


bool Database::FetchMovie()
{
    QSqlQuery tryFetch("SELECT `film_year`, `film_director`, `film_gatunek` , `film_rates`, `film_price`, `film_description`, `film_img` FROM `film`", database);

    if(tryFetch.exec())
    {
        if(tryFetch.size() > 0)
        {
            tryFetch.first();
            while(tryFetch.next())
            {
                Film nazwa;
                nazwa.cena = tryFetch.value("film_price").toFloat();
                nazwa.rok = tryFetch.value("film_year").toInt();
                nazwa.rezyser = tryFetch.value("film_director").toString();
                nazwa.gatunek = tryFetch.value("film_gatunek").toString();
                nazwa.oceny = tryFetch.value("film_rates").toFloat();
                nazwa.img = tryFetch.value("film_img").toString();
                nazwa.opis =tryFetch.value("film_description").toString();
                film.push_back(nazwa);
            }
            qDebug() << tryFetch.size();
            return true;
        }
    }
    else
    {
        qDebug() << tryFetch.lastError().text();
    }
    return false;

}

QVector<Film> Database::wczytajFilmy(){
    return this->film;
}
