#include "payment.h"
#include "ui_payment.h"

Payment::Payment(QWidget * parent, QString tytul, int cena) :
    QDialog(parent),
    ui(new Ui::Payment)
{
    ui->setupUi(this);
    ui->label98->setText("tytuł: " + tytul);
    ui->label99->setText("Do zapłaty: " + QString::number(cena));
}

Payment::~Payment()
{
    delete ui;
}
