#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>

#include "adminwindow.h"
#include "loginform.h"
#include "userwindow.h"
#include "loginform.h"
#include "database.h"
#include "user.h"

class AppManager : public QObject
{
    Q_OBJECT
public:
    AppManager();

private slots:
    void createWindow(Ui::Credentials &login);

private:
    QMainWindow* window = nullptr;
    LoginForm firstLogin;
    Database users;
    User logedIn;
};

#endif // APPMANAGER_H
