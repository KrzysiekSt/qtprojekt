QT       += core gui sql
QTPLUGIN += qsqlmysql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    adminwindow.cpp \
    appmanager.cpp \
    database.cpp \
    loginform.cpp \
    main.cpp \
    payment.cpp \
    user.cpp \
    userwindow.cpp

HEADERS += \
    adminwindow.h \
    appmanager.h \
    database.h \
    loginform.h \
    payment.h \
    user.h \
    userwindow.h

FORMS += \
    adminwindow.ui \
    loginform.ui \
    payment.ui \
    userwindow.ui

TRANSLATIONS += \
    QtProjekt_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations
CONFIG += console

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
