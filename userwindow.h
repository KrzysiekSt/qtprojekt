#ifndef USERWINDOW_H
#define USERWINDOW_H

#include <QMainWindow>

#include "database.h"
#include "appmanager.h"

namespace Ui {
class UserWindow;
}



class UserWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit UserWindow(Database *mysql);
    ~UserWindow();

private slots:
    void on_info5_clicked();

    void on_info4_clicked();

    void on_info3_clicked();

    void on_info2_clicked();

    void on_info1_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::UserWindow *ui;
    QString gatunek[5] =
    {
        "dramat",
        "horror",
        "Sci-fiction",
        "komedia",
        "thriller",
    };

    QString wybrany_film;
    int cena;
    Database *mySql;
    QVector<Film> wczytaneFilmy;


    QDialog* paymentWindow = nullptr;

};

#endif // USERWINDOW_H
