#ifndef USER_H
#define USER_H


class User
{
public:
    User();
    bool isAdmin();
    operator bool() const
    {
        return logedIn;
    }
    bool operator ==(const bool &arg) const;
    User& operator <<=(const bool &arg);
private:
    bool admin;
    bool logedIn;
};

#endif // USER_H
