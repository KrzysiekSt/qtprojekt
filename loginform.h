#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QDialog>

namespace Ui {
    class LoginForm;
    struct Credentials
    {
        QString email;
        QString password;
    };
}

class LoginForm : public QDialog
{
    Q_OBJECT

public:
    explicit LoginForm(QWidget *parent = nullptr);
    ~LoginForm();
signals:
    void createWindow(Ui::Credentials &login);

private slots:
    void on_buttonBox_accepted();

    void on_passwordInput_textChanged(const QString &arg1);

    void on_emailInput_textChanged(const QString &arg1);

private:
    Ui::LoginForm *ui;
    Ui::Credentials login;

};

#endif // LOGINFORM_H
