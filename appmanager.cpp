#include "appmanager.h"

AppManager::AppManager()
{
    if(users.handleAdminAccount())
    {
        QObject::connect(&firstLogin, &LoginForm::createWindow, this, &AppManager::createWindow);
        firstLogin.show();
    }
}


void AppManager::createWindow(Ui::Credentials &login)
{
    logedIn <<= users.tryLogin(login);
    if(!logedIn)
    {
        firstLogin.show();
        return;
    }

    if(users.isAdmin)
    {
        window = new AdminWindow();
    }
    else
    {
        users.FetchMovie();
        window = new UserWindow(&users);
    }
    window->show();
}
