#include "user.h"

User::User()
{
    admin = false;
    logedIn = true;
}

bool User::isAdmin()
{
    return admin;
}

bool User::operator ==(const bool &arg) const
{
    if(this->logedIn == arg)
    {
        return true;
    }
    return false;
}

User& User::operator <<=(const bool &arg)
{
    this->logedIn = arg;
}
