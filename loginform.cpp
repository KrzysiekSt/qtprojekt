#include "loginform.h"
#include "ui_loginform.h"

LoginForm::LoginForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginForm)
{
    ui->setupUi(this);
}

LoginForm::~LoginForm()
{
    delete ui;
}

void LoginForm::on_buttonBox_accepted()
{
    emit createWindow(login);
}

void LoginForm::on_passwordInput_textChanged(const QString &input)
{
    login.password = input;
}


void LoginForm::on_emailInput_textChanged(const QString &input)
{
    login.email = input;
}

