#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QVector>
#include <QSqlError>
#include <QtSql>
#include <QDebug>
#include <QString>

#include "loginform.h"

struct Film
{
    float cena;
    int rok;
    QString rezyser;
    QString gatunek;
    float oceny;
    QString opis;
    QString img;

};


class Database
{
public:
    Database();

    bool isAdmin;

    bool handleAdminAccount();
    bool tryLogin(Ui::Credentials &login);
    bool FetchMovie();
    QVector<Film> wczytajFilmy();

private:
    QSqlDatabase database;
    QVector<Film> film;
    bool haveAdmin();
    bool createAdmin();


};

#endif // DATABASE_H
